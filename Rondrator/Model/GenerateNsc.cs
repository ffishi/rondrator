﻿using Rondrator.Model.DataClasses;
using System;
using System.Collections.Generic;

namespace Rondrator.Model
{
  public static class GenerateNsc
  {
    public static Nsc RandomByDifficulty(float difficulty, NscTemplate nscTemplate, Region region, string gender, int weaponCount = 0, string name = "")
    {
      Random random = new Random();
      Dictionary<string, int> randomizedEigenschaften = RandomizeNumbers.GenerateEigenschaften(difficulty, random, nscTemplate);
      if(string.IsNullOrEmpty(name))
      {
        name = GenerateName.Exeute(region, gender);
      }
      Nsc randomNsc = new Nsc
      {
        Name = name,
        Mut = randomizedEigenschaften["Mut"],
        Klugheit = randomizedEigenschaften["Charisma"],
        Intuition = randomizedEigenschaften["Intuition"],
        Charisma = randomizedEigenschaften["Charisma"],
        Gewandheit = randomizedEigenschaften["Gewandheit"],
        Fingerfertigkeit = randomizedEigenschaften["Fingerfertigkeit"],
        Konstitution = randomizedEigenschaften["Konstitution"],
        Koerperkraft = randomizedEigenschaften["Koerperkraft"],
        Initiative = RandomizeNumbers.GenerateInitiative(difficulty, random),
        Geschwindigkeit = RandomizeNumbers.GenerateGeschwindigkeit(difficulty, random)
      };
      int numberOfKampfwerte = weaponCount;
      if (numberOfKampfwerte == 0)
      {
        numberOfKampfwerte = random.Next(1, 10);
      }
      for (int i = 0; i < numberOfKampfwerte; i++)
      {
        randomNsc.Kampfwerte.Add(CreateKampfwerte(difficulty, "Waffe " + (i + 1), random));
      }
      return randomNsc;
    }

    private static Kampfwerte CreateKampfwerte(float difficulty, string kampfwerteName, Random random)
    {
      Tuple<int, int> attackeParade = RandomizeNumbers.GenerateAttackeParade(difficulty, random);
      Kampfwerte kampfwerte = new Kampfwerte
      {
        Waffenname = kampfwerteName,
        Attacke = attackeParade.Item1,
        Parade = attackeParade.Item2,
        DamageWuerfel = RandomizeNumbers.GenerateKampwerteWuerfel(difficulty, random),
        DamageBonus = RandomizeNumbers.GenerateKampwerteBonus(difficulty, random)
      };
      return kampfwerte;
    }
  }
}
