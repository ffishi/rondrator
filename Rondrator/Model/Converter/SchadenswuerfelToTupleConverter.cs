﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Rondrator.Model.Converter
{
  public class SchadenswuerfelToTupleConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is Tuple<int, int> schadenswuerfelTuple)
      {
        string WuerfelString = "W6";
        if(schadenswuerfelTuple.Item2>0)
        {
          WuerfelString += String.Concat(" + ", schadenswuerfelTuple.Item2);
        }
        return string.Concat(schadenswuerfelTuple.Item1, WuerfelString);
      }
      return string.Empty;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string[] schadenswuerfel = ((string)value).Split(new String[] { "W6 + " }, StringSplitOptions.None);
      if (schadenswuerfel.Length == 2)
      {
        try
        {
          return new Tuple<int, int>(int.Parse(schadenswuerfel[0]), int.Parse(schadenswuerfel[1]));
        }
        catch (FormatException)
        {
          return new Tuple<int, int>(0, 0);
        }
      }
      return new Tuple<int, int>(0, 0);
    }
  }
}
