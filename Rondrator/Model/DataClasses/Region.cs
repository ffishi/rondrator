﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Rondrator.Model
{
  public class Region
  {
    [JsonProperty(PropertyName = "regionOderRasse")]
    public string RegionOderRasse { get; set; }
    [JsonProperty(PropertyName = "firstNames")]
    public FirstNames FirstNames { get; set; }
    [JsonProperty(PropertyName = "sohnVon")]
    public int SohnVon { get; set; }
    [JsonProperty(PropertyName = "lastNames")]
    public List<string> LastNames { get; set; }
  }
}
