﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rondrator.Model.DataClasses
{
 public class Kampfwerte
 {
  public string Waffenname { get; set; }
  public int Attacke { get; set; }
  public int Parade { get; set; }
  public int DamageWuerfel { get; set; }
  public int DamageBonus { get; set; }
  public Tuple<int, int> Damage
  {
   get
   {
    return new Tuple<int, int>(DamageWuerfel, DamageBonus);
   }
   set
   {
    DamageWuerfel = Damage.Item1;
    DamageBonus = Damage.Item2;
   }
  }
 }
}
