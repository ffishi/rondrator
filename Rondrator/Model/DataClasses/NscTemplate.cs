﻿using Newtonsoft.Json;

namespace Rondrator.Model
{
  public class NscTemplate
  {
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }
    [JsonProperty(PropertyName = "mutWeight")]
    public int MutWeight { get; set; } = 100;
    [JsonProperty(PropertyName = "klugheitWeight")]
    public int KlugheitWeight { get; set; } = 100;
    [JsonProperty(PropertyName = "intuitionWeight")]
    public int IntuitionWeight { get; set; } = 100;
    [JsonProperty(PropertyName = "charismaWeight")]
    public int CharismaWeight { get; set; } = 100;
    [JsonProperty(PropertyName = "gewandheitWeight")]
    public int GewandheitWeight { get; set; } = 100;
    [JsonProperty(PropertyName = "fingerfertigkeitWeight")]
    public int FingerfertigkeitWeight { get; set; } = 100;
    [JsonProperty(PropertyName = "konstitutionWeight")]
    public int KonstitutionWeight { get; set; } = 100;
    [JsonProperty(PropertyName = "koerperkraftWeight")]
    public int KoerperkraftWeight { get; set; } = 100;
  }
}
