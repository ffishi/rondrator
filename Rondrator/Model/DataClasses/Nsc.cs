﻿using System.Collections.ObjectModel;

namespace Rondrator.Model.DataClasses
{
  public class Nsc
  {
    public string Name { get; set; }
    public int Mut { get; set; }
    public int Klugheit { get; set; }
    public int Intuition { get; set; }
    public int Charisma { get; set; }
    public int Gewandheit { get; set; }
    public int Fingerfertigkeit { get; set; }
    public int Konstitution { get; set; }
    public int Koerperkraft { get; set; }
    public int Geschwindigkeit { get; set; }
    public int Initiative { get; set; }
    public ObservableCollection<Kampfwerte> Kampfwerte { get; set; } = new ObservableCollection<Kampfwerte>();
  }
}
