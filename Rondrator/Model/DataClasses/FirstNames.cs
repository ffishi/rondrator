﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Rondrator.Model
{
  public class FirstNames
  {
    [JsonProperty(PropertyName = "female")]
    public List<string> Female { get; set; }
    [JsonProperty(PropertyName = "male")]
    public List<string> Male { get; set; }
  }
}
