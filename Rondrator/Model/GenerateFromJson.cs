﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.IO;

namespace Rondrator.Model
{
  public static class GenerateFromJson
  {
    public static ObservableCollection<DataClass> Execute<DataClass>(string filePath)
    {
      ObservableCollection<DataClass> listDataObjects = new ObservableCollection<DataClass>();
      if (File.Exists(filePath))
      {
        using (StreamReader file = File.OpenText(filePath))
        {
          using (JsonTextReader reader = new JsonTextReader(file))
          {
            JArray jArray = (JArray)JToken.ReadFrom(reader);
            listDataObjects = jArray.ToObject<ObservableCollection<DataClass>>();
          }
        }
      }
      return listDataObjects;
    }
  }
}
