﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rondrator.Model.DataClasses;

namespace Rondrator.Model
{
  public class MainModel
  {
    public NscTemplate NscTemplate { get; internal set; }

    public Region Region { get; internal set; }

    public string Gender { get; internal set; }

    public MainModel()
    {

    }
    internal Nsc NewRandomNsc(float difficulty, int weaponCount = 0, string name = "")
    {
      return GenerateNsc.RandomByDifficulty(difficulty, NscTemplate, Region, Gender, weaponCount, name);
    }

    internal ObservableCollection<NscTemplate> InitNscTemplates()
    {
      return GenerateFromJson.Execute<NscTemplate>(Properties.Settings.Default.templatesFile);
    }

    internal ObservableCollection<Region> InitRegions()
    {
      return GenerateFromJson.Execute<Region>(Properties.Settings.Default.regionFile);
    }
  }
}
