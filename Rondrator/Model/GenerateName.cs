﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rondrator.Model
{
  public static class GenerateName
  {
    public static string Exeute(Region region, string gender)
    {
      Random random = new Random();
      bool isFemale = false;
      if (gender.Equals(Properties.Settings.Default.femaleString))
      {
        isFemale = true;
      }
      List<string> firstNameList;
      if (isFemale)
      {
        firstNameList = region.FirstNames.Female;
      }
      else
      {
        firstNameList = region.FirstNames.Male;
      }
      string firstName = firstNameList[random.Next(0, firstNameList.Count - 1)];
      string lastName;
      switch (region.SohnVon)
      {
        case 1: // 1: Zwergennamen
          string middlePart = ", Sohn des ";
          if (isFemale)
          {
            middlePart = ", Tochter der ";
          }
          lastName = string.Concat(middlePart, firstNameList[random.Next(0, firstNameList.Count - 1)]);
          break;
        case 2: // 2: Thorwalernamen
          string append = "son";
          if (isFemale)
          {
            append = "dottir";
          }
          lastName = string.Concat(firstNameList[random.Next(0, firstNameList.Count - 1)], append);
          break;
        default:
          lastName = region.LastNames[random.Next(0, region.LastNames.Count - 1)];
          break;
      }
      return string.Concat(firstName, " ", lastName);
    }
  }
}
