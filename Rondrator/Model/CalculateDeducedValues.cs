﻿using Rondrator.Model.DataClasses;
using System;

namespace Rondrator.Model
{
  public static class CalculateDeducedValues
  {
    public static int Lebensenergie(Nsc nsc)
    {
      return Properties.Settings.Default.Lebensenergiebasis + (int)Math.Round((double)(nsc.Konstitution + nsc.Konstitution + nsc.Koerperkraft) / 2);
    }

    public static int Astralenergie(Nsc nsc)
    {
      return Properties.Settings.Default.Astralenergiebasis + (int)Math.Round((double)(nsc.Mut + nsc.Intuition + nsc.Charisma) / 2);
    }

    public static int AttackeBasis(Nsc nsc)
    {
      return Properties.Settings.Default.Lebensenergiebasis + (int)Math.Round((double)(nsc.Mut + nsc.Gewandheit + nsc.Koerperkraft) / 5);
    }

    public static int ParadeBasis(Nsc nsc)
    {
      return Properties.Settings.Default.Lebensenergiebasis + (int)Math.Round((double)(nsc.Intuition + nsc.Gewandheit + nsc.Koerperkraft) / 5);
    }

    public static int FernkampfBasis(Nsc nsc)
    {
      return Properties.Settings.Default.Lebensenergiebasis + (int)Math.Round((double)(nsc.Intuition + nsc.Fingerfertigkeit + nsc.Koerperkraft) / 5);
    }

    public static int InitiativeBasis(Nsc nsc)
    {
      return Properties.Settings.Default.Lebensenergiebasis + (int)Math.Round((double)(nsc.Mut + nsc.Mut + nsc.Intuition + nsc.Gewandheit) / 5);
    }
  }
}
