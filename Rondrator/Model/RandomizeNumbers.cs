﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Rondrator.Model
{
  public static class RandomizeNumbers
  {
    public static Dictionary<string, int> GenerateEigenschaften(double difficulty, Random random, NscTemplate nscTemplate)
    {
      Dictionary<string, int> eigenschaften = new Dictionary<string, int>();
      double[] eigenschaftValues = GenerateRandomNumbers(
       (uint)GlobalConstants.Eigenschaften.Count,
       Properties.Settings.Default.MinBaseEigenschaft,
       Properties.Settings.Default.MaxBaseEigenschaft,
       GlobalConstants.MaxBaseEigenschaftenSum,
       random
       );
      List<double> eigenschaftenValuesSorted = new List<double>(eigenschaftValues);
      eigenschaftenValuesSorted.Sort();
      Dictionary<string, int> eigenschaftenTemplateWeight = new Dictionary<string, int>();
      PropertyInfo[] properties = nscTemplate.GetType().GetProperties();
      foreach (var property in properties)
      {
        if (property.PropertyType == typeof(int) && property.GetValue(nscTemplate) != null)
        {
          eigenschaftenTemplateWeight.Add(property.Name.Replace("Weight", ""), (int)property.GetValue(nscTemplate));
        }
      }
      int i = 0;
      foreach (KeyValuePair<string, int> item in eigenschaftenTemplateWeight.OrderBy(key => key.Value))
      {
        eigenschaften.Add(item.Key, (int)(eigenschaftenValuesSorted[i] * difficulty / GlobalConstants.MaxBaseEigenschaftenSum));
        i++;
      }
      return eigenschaften;
    }

    public static int GenerateKampwerteWuerfel(double difficulty, Random random)
    {
      return (int)Math.Round((random.Next(1, 3) * difficulty / GlobalConstants.MaxBaseEigenschaftenSum));
    }

    public static int GenerateKampwerteBonus(double difficulty, Random random)
    {
      return (int)(random.Next(1, 5) * difficulty / GlobalConstants.MaxBaseEigenschaftenSum);
    }

    internal static int GenerateGeschwindigkeit(float difficulty, Random random)
    {
      return GenerateSmallValue(difficulty, 40, random);
    }

    internal static int GenerateInitiative(float difficulty, Random random)
    {
      return GenerateSmallValue(difficulty, 20, random);
    }

    internal static Tuple<int, int> GenerateAttackeParade(float difficulty, Random random)
    {
      return new Tuple<int, int>(
       GenerateSmallValue(difficulty, 20, random),
       GenerateSmallValue(difficulty, 20, random)
       );
    }

    private static int GenerateSmallValue(float difficulty, int divisor, Random random)
    {
      return Properties.Settings.Default.MinBaseEigenschaft + random.Next(0, (int)(difficulty / divisor));
    }

    public static double[] GenerateRandomNumbers(uint values, double minimum, double maximum, double sum, Random generator)
    {
      if (values == 0)
        throw new InvalidOperationException($"Cannot create list of zero numbers.");
      if (minimum * values > sum)
        throw new InvalidOperationException($"The minimum value ({minimum}) is too high.");
      if (maximum * values < sum)
        throw new InvalidOperationException($"The maximum value ({maximum}) is too low.");
      if (minimum > maximum)
        throw new InvalidOperationException($"The maximum value ({maximum}) is lower than the minimum value ({minimum}).");
      if (generator == null)
        generator = new Random();

      var numberList = new double[values];

      for (var index = 0; index < values - 1; index++)
      {
        var rest = numberList.Length - (index + 1);

        var restMinimum = minimum * rest;
        var restMaximum = maximum * rest;

        minimum = Math.Max(minimum, sum - restMaximum);
        maximum = Math.Min(maximum, sum - restMinimum);
        var newRandomValue = generator.NextDouble() * (maximum - minimum) + minimum;
        numberList[index] = newRandomValue;
        sum -= newRandomValue;
      }

      numberList[values - 1] = sum;

      return numberList;
    }

  }
}
