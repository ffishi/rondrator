﻿using System.Collections.Generic;

namespace Rondrator.Model
{
 public static class GlobalConstants
 {
  public static List<string> Eigenschaften { get; set; } = new List<string> {
        "Mut",
        "Klugheit",
        "Intuition",
        "Charisma",
        "Gewandheit",
        "Fingerfertigkeit",
        "Konstitution",
        "Koerperkraft"
      };
  public static int MaxBaseEigenschaftenSum { get; set; } = 90;
 }
}
