﻿using Rondrator.ViewModel;
using System;
using System.Windows.Input;

public class NewNameButton : ICommand
{
  private readonly MainViewModel viewModel;
  public NewNameButton(MainViewModel _viewModel)
  {
    viewModel = _viewModel;
  }

  public event EventHandler CanExecuteChanged;

  public bool CanExecute(object parameter)
  {
    return true;
  }
  public void Execute(object parameter)
  {
    viewModel.NewRandomNameCommand();
  }
}
