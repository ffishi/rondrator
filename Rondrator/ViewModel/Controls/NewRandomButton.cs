﻿using Rondrator.ViewModel;
using System;
using System.Windows.Input;

public class NewRandomButton : ICommand
{
  private readonly MainViewModel viewModel;
  public NewRandomButton(MainViewModel _viewModel)
  {
    viewModel = _viewModel;
  }

  public event EventHandler CanExecuteChanged;

  public bool CanExecute(object parameter)
  {
    return true;
  }
  public void Execute(object parameter)
  {
    viewModel.NewRandomNscCommand();
  }
}