﻿using Rondrator.Model;
using Rondrator.Model.DataClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Rondrator.ViewModel
{
  public class MainViewModel : ViewModelBase
  {
    #region Properties
    private ObservableCollection<Nsc> _GeneratedNscs;
    public ObservableCollection<Nsc> GeneratedNscs
    {
      get { return _GeneratedNscs; }
      set
      {
        _GeneratedNscs = value;
        NotifyPropertyChanged("GeneratedNscs");
      }
    }
    private ObservableCollection<NscTemplate> _NscTemplates;
    public ObservableCollection<NscTemplate> NscTemplates
    {
      get { return _NscTemplates; }
      set
      {
        _NscTemplates = value;
        NotifyPropertyChanged("NscTemplates");
      }
    }
    private int _Lebenspunkte;
    public int Lebenspunkte
    {
      get { return _Lebenspunkte; }
      set
      {
        _Lebenspunkte = value;
        NotifyPropertyChanged("Lebenspunkte");
      }
    }
    private int _Astralpunkte;
    public int Astralpunkte
    {
      get { return _Astralpunkte; }
      set
      {
        _Astralpunkte = value;
        NotifyPropertyChanged("Astralpunkte");
      }
    }
    private NscTemplate _SelectedNscTemplate;
    public NscTemplate SelectedNscTemplate
    {
      get { return _SelectedNscTemplate; }
      set
      {
        _SelectedNscTemplate = value;
        Model.NscTemplate = _SelectedNscTemplate;
        CurrentNsc = Model.NewRandomNsc(_Difficulty, CurrentNsc.Kampfwerte.Count);
        UpdateDeductedValues();
        NotifyPropertyChanged("SelectedNscTemplate");
      }
    }

    private float _Difficulty;
    public float Difficulty
    {
      get { return _Difficulty; }
      set
      {
        if (value != _Difficulty)
        {
          _Difficulty = value;
          CurrentNsc = Model.NewRandomNsc(_Difficulty, CurrentNsc.Kampfwerte.Count, CurrentNsc.Name);
          UpdateDeductedValues();
          NotifyPropertyChanged("Difficulty");
        }
      }
    }
    private Nsc _CurrentNsc;
    public Nsc CurrentNsc
    {
      get { return _CurrentNsc; }
      set
      {
        _CurrentNsc = value;
        NotifyPropertyChanged("CurrentNsc");
      }
    }
    private ObservableCollection<Region> _Regions;
    public ObservableCollection<Region> Regions
    {
      get { return _Regions; }
      set
      {
        _Regions = value;
        NotifyPropertyChanged("Regions");
      }
    }
    private Region _SelectedRegion;
    public Region SelectedRegion
    {
      get { return _SelectedRegion; }
      set
      {
        _SelectedRegion = value;
        Model.Region = value;
        CurrentNsc.Name = GenerateName.Exeute(SelectedRegion, SelectedGender);
        NotifyPropertyChanged("SelectedRegion");
        NotifyPropertyChanged("CurrentNsc");
      }
    }
    private List<string> _Gender = new List<string>() { "männlich", "weiblich" };
    public List<string> Gender
    {
      get { return _Gender; }
      set
      {
        _Gender = value;
        NotifyPropertyChanged("Gender");
      }
    }
    public string _SelectedGender;
    public string SelectedGender {
      get { return _SelectedGender; }
      set
      {
        _SelectedGender = value;
        Model.Gender = value;
        CurrentNsc.Name = GenerateName.Exeute(SelectedRegion, SelectedGender);
        NotifyPropertyChanged("SelectedGender");
        NotifyPropertyChanged("CurrentNsc");
      }
    }

    public MainModel Model { get; set; }
    private readonly NewRandomButton NewRandomButton;
    private readonly NewNameButton NewNameButton;
    #endregion Properties

    #region Constructor
    public MainViewModel()
    {
      CurrentNsc = new Nsc();
      Model = new MainModel();
      GeneratedNscs = new ObservableCollection<Nsc>();
      _SelectedGender = Gender[0];
      Model.Gender = SelectedGender;
      Regions = Model.InitRegions();
      if (Regions.Any())
      {
        _SelectedRegion = Regions[0];
        Model.Region = SelectedRegion;
      }
      NscTemplates = Model.InitNscTemplates();
      if (NscTemplates.Any())
      {
        _SelectedNscTemplate = NscTemplates[0];
        Model.NscTemplate = SelectedNscTemplate;
      }
      CurrentNsc = Model.NewRandomNsc(Properties.Settings.Default.InitialDifficulty);
      NewRandomButton = new NewRandomButton(this);
      NewNameButton = new NewNameButton(this);
      Difficulty = Properties.Settings.Default.InitialDifficulty;
      UpdateDeductedValues();
    }
    #endregion Constructor

    #region commands
    public void NewRandomNscCommand()
    {
      CurrentNsc = Model.NewRandomNsc(Difficulty);
      UpdateDeductedValues();
    }
    public ICommand NewRandomButtonClick
    {
      get
      {
        return NewRandomButton;
      }
    }
    public void NewRandomNameCommand()
    {
      CurrentNsc.Name = GenerateName.Exeute(SelectedRegion, SelectedGender);
      NotifyPropertyChanged("CurrentNsc");
    }
    public ICommand NewRandomNameClick
    {
      get
      {
        return NewNameButton;
      }
    }
    #endregion commands

    #region Functions
    public void UpdateDeductedValues()
    {
      Lebenspunkte = CalculateDeducedValues.Lebensenergie(CurrentNsc);
      Astralpunkte = CalculateDeducedValues.Astralenergie(CurrentNsc);
    }

    #endregion Functions
  }
}
